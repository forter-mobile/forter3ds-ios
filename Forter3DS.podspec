Pod::Spec.new do |s|
  s.name             = 'Forter3DS'
  s.version          = '1.3.5'
  s.summary          = 'Forter3DS - iOS SDK'
  s.description      = <<-DESC
                        Forter's 3DS solution for 3DS1 and 3DS2.
                       DESC
  s.homepage         = 'https://www.forter.com'
  s.license          = { :type => "Commercial", :text => "Forter LTD Copyright 2016-2020" }
  s.author           = { "Forter Growth Engineering" => "integration-support@forter.com" }
  s.source           = { :git => "https://bitbucket.org/forter-mobile/forter3ds-ios.git" }
  s.platform         = :ios
  s.requires_arc     = true
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'xcframeworks/Forter3DS.xcframework', 'xcframeworks/ThreeDS_SDK.xcframework'
  s.ios.source_files = 'Headers/*.h'
  s.ios.public_header_files = 'Headers/*.h'
  s.resource_bundles = {'Forter3DS' => ['Sources/Forter3DSWrapper/PrivacyInfo.xcprivacy']}
end
