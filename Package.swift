// swift-tools-version:5.3
import PackageDescription

let package = Package(
  name: "Forter3DS",
  platforms: [
    .iOS(.v11)
  ],
  products: [
    .library(
      name: "Forter3DS",
      targets: ["Forter3DSWrapper"])
  ],
  targets: [
    .target(
      name: "Forter3DSWrapper",
      dependencies: [.target(name: "Forter3DS"), .target(name: "ThreeDS_SDK")],
      path: ".",
      resources: [.process("Sources/Forter3DSWrapper/PrivacyInfo.xcprivacy")],
      cSettings: [
        .headerSearchPath("Headers"),
      ]
    ),
    .binaryTarget(
      name: "Forter3DS",
      path: "xcframeworks/Forter3DS.xcframework"
    ),
    .binaryTarget(
      name: "ThreeDS_SDK",
      path: "xcframeworks/ThreeDS_SDK.xcframework"
    )
  ]
)
