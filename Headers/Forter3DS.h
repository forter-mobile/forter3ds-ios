//
//  Forter3DS.h
//  Forter3DS
//
//  Created by Forter dev on 28/07/2019.
//  Copyright © 2019 Forter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Forter3DSPublicConstants.h"
#import "FTR3DSCustomization.h"
#import "Forter3DSDelegate.h"
#import "FTRTransaction.h"

@interface Forter3DS : NSObject

/**
 * setup SDK with default customization
 */
+ (void)setup;

/**
 * setup SDK with customized UI components
 * @param customization FTR3DSCustomization object initialized with labelCustomization, textBoxCustomization and toolbarCustomization
 */
+ (void)setupWithCustomization:(nonnull FTR3DSCustomization *)customization;

/**
 * Creates an instance of FTRTransaction which contains the data to perform the transaction
 * @param dsId Directory Server identifier, can be null in case it's 3DS1 flow
 * @param version 3DS version
 * Examples: "1.0.2", "2.0.1"
 * @return initialized FTRTransaction object
 */



+ (nullable FTRTransaction *)createTransactionWithDsId:(nullable NSString *)dsId version:(nullable NSString *)version;

/// Initiate the challenge process for a 3ds transaction
///
/// When `doChallenge` method is called, the control of the app is passed to the SDK
///
/// @param version 3ds version
/// @param acsUrl URL to the Issuer ACS page
/// @param creq Card holder authentication data required for presenting the challenge
/// @param transaction FTRTransaction object contains required data for 3ds2 flows
/// @param acsRefNumber Unique identifier assigned by the EMVCo
/// @param acsSignedContent A string created by the ACS contains the JWS object for the ARes
/// @param acsTransId Unique transaction identifier assigned by the ACS
/// @param transactionId Universally unique transaction identifier assigned by the 3DS Server
/// @param viewController Current presenting view controller
/// @param delegate Callback for challenge result
+ (void)doChallengeWithVersion:(nonnull NSString *)version merchantId:(nullable NSString *)merchantId
                    withACSUrl:(nullable NSString *)acsUrl creq:(nullable NSString *)creq
                ftrTransaction:(nullable FTRTransaction *)transaction acsRefNumber:(nullable NSString *)acsRefNumber
              acsSignedContent:(nullable NSString *)acsSignedContent acsTransId:(nullable NSString *)acsTransId
          threeDSServerTransId:(nonnull NSString *)transactionId currentVC:(nonnull UIViewController *)viewController
                     callbacks:(nonnull id <Forter3DSDelegate>)delegate;

/**
 * print current SDK version
 */
+ (void)printSDKVersion;

/**
 * Enable developer logs for integration testing
 */
+ (void)enableDevLogs:(bool)enabled;

/**
 * load test servers to enable 3ds2 flows not in development mode
 */
+ (void)loadTestServers;

@end
