//
//  FTR3DSTextBoxCustomization.h
//  Forter3DS
//
//  Created by Moshe Krush on 01/03/2020.
//  Copyright © 2020 Forter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTR3DSTextBoxCustomization : NSObject

/**
 The setBorderWidth method shall set the width of the text box border.

 @param borderWidth Int width (integer value) for the text border width.

 */
- (void)setBorderWidth:(NSInteger)borderWidth;

/**
 The setBorderColor method shall set the color for the border of the text box.

 @param hexColorCode String Color code in Hex format. For example, the color code can be "#999999".

 */
- (void)setBorderColor:(nonnull NSString *)hexColorCode;

/**
 The setCornerRadius method shall set the radius for the text box corners.

 @param cornerRadius radius (integer value) for the text box corners.

 */
- (void)setCornerRadius:(NSInteger) cornerRadius;

/**
 The getBorderWidth method shall return the width of the text box border.

 @return border width of text field

 */
- (NSInteger)getBorderWidth;

/**
 The getBorderColor method shall return the color of the text box border.

 @return border color of text field

 */
- (nonnull NSString *)getBorderColor;

/**
 The getCornerRadius method shall return the radius for the text box corners.

 @return corner radius of text field

 */
- (NSInteger)getCornerRadius;


@end
