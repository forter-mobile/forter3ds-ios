//
//  Forter3DSPublicConstants.h
//  Forter3DS
//
//  Created by Moshe Krush on 08/03/2020.
//  Copyright © 2020 Forter. All rights reserved.
//

#define FTR_3DS_SDK_VERSION_STRING_NAME @"1.3.5"
#define FTR_3DS_SDK_VERSION_STRING_BUILD @"1"
#define FTR_3DS_SDK_VERSION_SIGNATURE @"6044da7fb2191980a0212fde073dd336"

#define FTR_3DS_SDK_LOG_LEVEL_OFF 0
#define FTR_3DS_SDK_LOG_LEVEL_ERROR 1
#define FTR_3DS_SDK_LOG_LEVEL_WARN 2
#define FTR_3DS_SDK_LOG_LEVEL_DEBUG 3
#define FTR_3DS_SDK_LOG_LEVEL_INFO 4
#define FTR_3DS_SDK_LOG_LEVEL_VERBOSE 5
