//
//  FTR3DSCustomization.h
//  Forter3DS
//
//  Created by Moshe Krush on 01/03/2020.
//  Copyright © 2020 Forter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FTR3DSLabelCustomization.h"
#import "FTR3DSTextBoxCustomization.h"
#import "FTR3DSToolbarCustomization.h"
#import "FTR3DSButtonCustomization.h"
#import "FTR3DSButtonType.h"

@interface FTR3DSCustomization : NSObject

/**
 Init method that sets the label, textBox (text field), and toolbar (UINavigationBar) properties

 @param labelCustomization a LabelCustomization object that sets label customization for the SDK
 @param textBoxCustomization a TextBoxCustomization object that sets text field customizations in the SDK
 @param toolbarCustomization a ToolbarCustomization object that sets the navigation bar customizations in the SDK
 @return FTR3DSCustomization object with all properties initialized
 */
- (nonnull FTR3DSCustomization *)initWithLabelCustomization:(nonnull FTR3DSLabelCustomization *)labelCustomization
                                    textBoxCustomization:(nonnull FTR3DSTextBoxCustomization *)textBoxCustomization
                                    toolbarCustomization:(nonnull FTR3DSToolbarCustomization *)toolbarCustomization;

/**
 The setToolbarCustomization method shall accept a ToolbarCustomization object. The 3DS SDK uses this object for customizing navigation bars

 @param toolbarCustomization a toolbar customization
 */
- (void)setToolbarCustomization:(nonnull FTR3DSToolbarCustomization *)toolbarCustomization;

/**
 The setLabelCustomization method shall accept a LabelCustomization object. The 3DS SDK uses this object for customizing labels.

 @param labelCustomization a label customization
 */
- (void)setLabelCustomization:(nonnull FTR3DSLabelCustomization *)labelCustomization;

/**
 The setTextBoxCustomization method shall accept a TextBoxCustomization object. The 3DS SDK uses this object for customizing text fields.

 @param textBoxCustomization TextBoxCustomization
 */
- (void)setTextBoxCustomization:(nonnull FTR3DSTextBoxCustomization *)textBoxCustomization;

/**
 The setButtonCustomization method shall accept a ButtonCustomization object along with a predefined button type. The 3DS SDK uses this object for customizing buttons.

 @param buttonCustomization ButtonCustomization
 @param buttonType String
 */
- (void)setButtonCustomization:(nonnull FTR3DSButtonCustomization *)buttonCustomization
                withButtonType:(FTR3DSButtonType)buttonType;

/**
 This method is a variation of the setButtonCustomization method. The setButtonCustomization method shall accept a ButtonCustomization object and an implementer-specific button type. The 3DS SDK uses this object for customizing buttons.

 Note: This method shall be used when the SDK implementer wants to use a button type that is not included in the predefined Enum NTButtonType. The SDK implementer shall maintain a dictionary of buttons passed via this method for use during customization.

 @param buttonCustomization ButtonCustomization
 @param buttonType String
 */
- (void)setCustomButtonCustomization:(nonnull FTR3DSButtonCustomization *)buttonCustomization
                          withString:(nonnull NSString *)buttonType;
@end
