//
//  FTR3DSButtonType.h
//  Forter3DS
//
//  Created by Moshe Krush on 01/03/2020.
//  Copyright © 2020 Forter. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FTR3DSButtonType) {

    /**
     a `verify` button
     */
    FTR3DSButtonTypeVerify,

    /**
     a `continue` button
     */
    FTR3DSButtonTypeContinue,

    /**
     a `next` button
     */
    FTR3DSButtonTypeNext,

    /**
     a `cancel` button
     */
    FTR3DSButtonTypeCancel,

    /**
     a `resend` button
     */
    FTR3DSButtonTypeResend
};
