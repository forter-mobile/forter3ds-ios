//
//  FTR3DSButtonCustomization.h
//  Forter3DS
//
//  Created by Moshe Krush on 01/03/2020.
//  Copyright © 2020 Forter. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FTR3DSButtonCustomization : NSObject

/**
 The setBackgroundColor method shall set the background color for the button.

 @param hexColorCode String Color code in Hex format. For example, the color code can be "#999999".
 // buttonCustomization.setBackgroundColor("2f2f2f")
 */
- (void)setBackgroundColor:(nonnull NSString *)hexColorCode;

/**
 The setCornerRadius method shall set the radius for the button corners.

 @param cornerRadius Int Radius (integer value) for the button corners.
 */
- (void)setCornerRadius:(NSInteger)cornerRadius;

/**
 The getBackgroundColor method shall return the background color for the button.
 */
- (nonnull NSString *)getBackgroundColor;

/**
 The getCornerRadius method shall return the radius for the button corners.
 */
- (NSInteger)getCornerRadius;

@end

NS_ASSUME_NONNULL_END
