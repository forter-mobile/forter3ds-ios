//
//  Forter3DSDelegate.h
//  Forter3DS
//
//  Created by Forter dev on 21/05/2019.
//  Copyright © 2019 Forter. All rights reserved.
//

#ifndef Forter3DSDelegate_h
#define Forter3DSDelegate_h

@protocol Forter3DSDelegate

/**
 * callback method for a completed challenge
 * This method will be called upon challenge completion wether the input is correct or not
 * @param dictionary NSDictionary with encoded challenge result.
 * example: {"cres": NSString};
 */
- (void)onChallengeFinish:(nullable NSDictionary *)dictionary;

/**
 * callback method for a failed challenge
 * This method will be called if the challenge failed due to an internal issue and not because of a unsuccessfull code
 * @param dictionary NSDictionary contains an optional value of NSError object that caused the challenge to fail
 * examples: {"error": NSError};
 */
- (void)onChallengeFailed:(nullable NSDictionary *)dictionary;

@end

#endif /* Forter3DSDelegate_h */
