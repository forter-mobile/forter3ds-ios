//
//  FTR3DSLabelCustomization.h
//  Forter3DS
//
//  Created by Moshe Krush on 01/03/2020.
//  Copyright © 2020 Forter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTR3DSLabelCustomization : NSObject

- (void)setHeadingTextColor:(nonnull NSString *)hexColorCode;

- (void)setHeadingTextFontName:(nonnull NSString *) fontName;

- (void)setHeadingTextFontSize:(NSInteger) fontSize;

- (nonnull NSString *)getHeadingTextFontName;

- (nonnull NSString *)getHeadingTextColor;

- (NSInteger)getHeadingTextFontSize;

@end
