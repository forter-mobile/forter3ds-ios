//
//  FTR3DSToolbarCustomization.h
//  Forter3DS
//
//  Created by Moshe Krush on 01/03/2020.
//  Copyright © 2020 Forter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FTR3DSToolbarCustomization : NSObject

/**
 The setBackgroundColor method shall set the background color for the navigation bar.
 
 @param hexColorCode Color code in Hex format. For example, the color code can be "#999999".
 */
- (void)setBackgroundColor:(nonnull NSString *)hexColorCode;

/**
 The setHeaderText method shall set the title of the navigation bar.
 
 @param headerText Text for the title
 
 */
- (void)setHeaderText:(nonnull NSString *)headerText;

/**
 The setButtonText method shall set the button text for the cancel button
 
 @param buttonText Text for the button
 
 */
- (void)setButtonText:(nonnull NSString *)buttonText;

/**
 The getBackgroundColor method shall return the background color for the navigation bar.

 @return the hex color code for the navigation bar
 */
- (nonnull NSString *)getBackgroundColor;

/**
 The getHeaderText method shall return the title for the navigation bar.

 @return the text for the navigation bar's title
 */
- (nullable NSString *)getHeaderText;

/**
 The getButtonText method shall return the button text of the toolbar.
 
  @return the text for the cancel button
 */
- (nullable NSString *)getButtonText;

@end
